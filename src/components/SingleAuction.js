import React from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import VisibilitySharpIcon from '@material-ui/icons/VisibilitySharp';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Tooltip from '@material-ui/core/Tooltip';
import { CardSize , CardMargin , ResponsiveScreenSize }  from '../utils';



const useStyles = makeStyles({
    root: {
      width: 250,
      height:300,
      marginTop:20,
      boxShadow: '0 0 4px rgb(0 0 0 / 64%)',
    },
    destination:{
            fontSize: '1.2rem',
            fontWeight: 'bold'
    },
    actions: {
        justifyContent:'center'
    },
    media:{
        height: 0,
        paddingTop: '56.25%', // 16:9
        backgroundColor:'lightgray' // placeholder for images...
    },
    viewers:{
        display:'inline-flex',
        justifyContent:'center',
        alignItems: 'center',
        margin:"0 5px"
    },
    icon:{
        marginRight:4
    }
  });


export default React.memo(function SingleAuction({realIndex , index , data  }) {
    const classes = useStyles();
    const rootStyle = window.innerWidth < ResponsiveScreenSize ? { 
      left: '50%',
      transform: 'translateX(-50%)',
      position:'absolute',
      top:`${index * (CardSize + CardMargin)}px`
    } : null;

    return (
        <Card className={classes.root} data-realindex={realIndex} data-index={index} style={rootStyle}>
            <CardMedia
                    className={classes.media}
                    image={data.imageUrl} 
                    title={data.inboundId}
                />
         
            <CardContent>
                    <Typography className={classes.destination} variant="h5" component="h1">
                          From {data.outboundId} to {data.inboundId}
                     </Typography>     
                     <Typography variant="body1" component="div">
                        
                        <Tooltip title="Viewers" placement="top">
                                <Typography className={classes.viewers} variant="inherit" component="p">
                                            <VisibilitySharpIcon className={classes.icon}></VisibilitySharpIcon> {data.viewersCount}
                                </Typography>
                        </Tooltip> 

                        <Tooltip title="Start from" placement="top">
                                <Typography className={classes.viewers} variant="inherit" component="p">
                                                 <MonetizationOnIcon className={classes.icon}></MonetizationOnIcon> {data.currentMinPrice}{data.currency}    
                                </Typography>    
                        </Tooltip>                                
                    </Typography>  
            </CardContent>
            <CardActions className={classes.actions}>
                        
                        <Button variant="contained" color="secondary">Show More</Button>
                          
                    
            </CardActions>
        </Card>
    )
});
 