export const CardSize = 300;
export const CardMargin = 20;

export const ResponsiveScreenSize = 800;

export const URL = 'https://static.bidflyer.com.s3.amazonaws.com/promotional/test.json';

export const sliceArray = (arr , chunkSize) =>{
  const res = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
      const chunk = arr.slice(i, i + chunkSize);
      res.push(chunk);
  }
  return res;
}