import './App.css';
import { useState , useEffect , useRef , useMemo  } from 'react';
import SingleAuction from './components/SingleAuction';
import data from './data/auctions.json';
import { CardSize , CardMargin  , sliceArray , ResponsiveScreenSize  , URL }  from './utils';


function App() {
  
  const [ auctionsArray , setAuctions ] = useState([])
  const [ indexes ,  setIndexes ] = useState({ start: 0 , end: 0 });
  const root = useRef(null)
  const tripleAuctions = useMemo( () => sliceArray(auctionsArray, 3)  , [auctionsArray]);



  useEffect(() => {

        const calculateHeight = event =>{
            const currentHeight = root.current.clientHeight;
            let targetHieght = 0;
            if(window.innerWidth < ResponsiveScreenSize){
              targetHieght =  (auctionsArray.length + 1)  * CardSize + CardMargin;
            }else{
              targetHieght  = (Math.ceil(auctionsArray.length  / 3) + 1)  * CardSize + CardMargin;
           }

          if(currentHeight !== targetHieght){
            root.current.style.height  = targetHieght + 'px';
          }
           root.current.parentElement.scrollTo(0,0);
           setIndexes({start: 0 , end: Math.ceil(window.innerHeight / CardSize) });
      }

      // fetch(URL)
      //   .then(result => result.json()) 
      //   .then(data =>{
      
      if(auctionsArray.length < 1){
      
      
      // create fake items...
      const tempAuctions = [...data];
      tempAuctions.forEach((item , idx) =>{
        item.key = `${item.id}_${idx}`;
        item.realIndex = idx;
      })
      let currentIdx = tempAuctions.length;
      while(currentIdx < 100){
              
            const randoItem = {...tempAuctions[ Math.floor( Math.random() * tempAuctions.length ) ]};
            randoItem.key = `${randoItem.id}_${currentIdx}`; 
            randoItem.realIndex = currentIdx;
            randoItem.imageUrl = `https://picsum.photos/200?${currentIdx}`;
            tempAuctions.push(randoItem);
            currentIdx++;
      }
      
        root.current.parentElement.style.height =  window.innerHeight + 'px';
        root.current.style.height  = (window.innerWidth < ResponsiveScreenSize ? (tempAuctions.length + 1) : Math.ceil(tempAuctions.length/3)+1 ) * CardSize + CardMargin + 'px';
      

      console.log('total items: ', tempAuctions.length);
      setAuctions(tempAuctions);
      setIndexes({start: 0 , end: Math.ceil(window.innerHeight / CardSize) });
        
      
      }
   
   
      
      // })


      window.addEventListener('resize'  , calculateHeight , false);

    return () => {
      
      window.removeEventListener('resize'  , calculateHeight , false);
    }
  }, [auctionsArray.length])


  const doVirtualScroll = () =>{
    // Only render what user can see...for maximum performance.
    const startIndex = Math.floor(root.current.parentElement.scrollTop / CardSize);
    const maxItems = Math.ceil(window.innerHeight / CardSize);

    setIndexes({start: startIndex , end: startIndex + maxItems });
  }


  const getSingleRowDesktop =  (idx , AuctionArr) =>{
    const rootStyle = window.innerWidth >= ResponsiveScreenSize ? { 
      left: '50%',
      transform: 'translateX(-50%)',
      position:'absolute',
      top:`${idx * (CardSize + CardMargin)}px`
    } : null;

    return <div style={rootStyle} key={AuctionArr[0].key} data-row-index={idx} className="single-auction-row" >{AuctionArr.map((Auction, index) => <SingleAuction key={Auction.key} realIndex={Auction.realIndex} index={index} data={Auction}  />)}</div>;
  } 



   
  const { start , end } = indexes;

  return (
    <div className="App" onScroll={doVirtualScroll}>
          <div className="auction-list" ref={root}>
              {window.innerWidth < ResponsiveScreenSize ? auctionsArray?.slice(start , end).map( (Auction , idx ) => <SingleAuction key={Auction.key} realIndex={Auction.realIndex} index={idx} data={Auction}  /> ) :
              tripleAuctions?.slice(start , end).map((AuctionArr , idx) => getSingleRowDesktop(idx , AuctionArr) )}
          </div>
    </div>
  );
}

export default App;